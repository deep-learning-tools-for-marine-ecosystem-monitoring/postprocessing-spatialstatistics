## Postprocessing - SpatialStatistics

Here you can find the scripts to calculate and plot the spatial statistics from a masked image (array with pixel values denoting their class), which is the output for the semantic segmentation inference tool. These scripts were used in the manuscript: _"James RK, Daniels F, Chauhan A, Wicaksono P, Hafizt M, Setiawan D & Christianen MJA. Combining deep learning approaches to monitor ecosystem functioning and infer resilience: automated detection of vegetation patterns and megaherbivores from drone imagery"_. 

1. **PatchinessStatistics.ipynb**: A python script that uses a sliding window analysis to subset a full image into a number of smaller images in which  spatial statistics such as mean patch size and patch density are then calculated for. The window size is set by the user and should reflect an area that is large enough to get a good sample size of patches to caluclate the mean etc. This script contains code to calculate the new coordinates for each subset image and to remove images that overlap. The calculation of the new coordinates does require the heading (direction in degrees) of the drone when the photo was taken, for some drones this is automatically saved in a log file (Example provided 'Example_LogFile.csv), while other drones this is not saved and might need to be manually calculated from the drone flight path (Example provide 'Example_HeadingFile.csv). This script can be run in JupyterLab or JupyterNotebook.

2. **PlottingInterpolationMaps.R**: An R script to interpolate the spatial statistics onto a spatial region so a coloured map can be produced to show the variation in the spatial statistics. 

For support running these scripts you can contact rebecca.james@ulb.be

## License: 
CC BY: Open access with attribution

